package nerds;

import org.testng.Assert;
import org.testng.annotations.Test;

public class TestHackerNerd {

	
	@Test (groups = {"batch1", "integration_tests"})
	public void testTooMuchCaffine() throws Exception {
		/////////////////
		// your actual test would go here
		/////////////////
		
		Assert.assertTrue(true);
	}
	
	@Test (groups = {"batch2", "integration_tests"})
	public void testIsLargelyNocturnal() throws Exception {
		/////////////////
		// your actual test would go here
		/////////////////
		
		Assert.assertTrue(true);
	}
	
	@Test (groups = {"batch1", "integration_tests"})
	public void testFueledByJunkFood() throws Exception {
		/////////////////
		// your actual test would go here
		/////////////////
		
		Assert.assertTrue(true);
	}
	
	@Test (groups = {"batch2", "integration_tests", "smoke_test"})
	public void testCanFixAnyBug() throws Exception {
		/////////////////
		// your actual test would go here
		/////////////////
		
		Assert.assertTrue(true);
	}
}
